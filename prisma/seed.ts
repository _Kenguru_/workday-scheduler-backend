import { PrismaClient } from "@prisma/client";
import faker from "faker";

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const prismaClient = new PrismaClient();

const roomNumbers = [100, 200, 300]
  .map((start) => [...Array(10)].map((_, i) => start + i))
  .flat();

main().catch(console.error);

async function main() {
  await prismaClient.room.deleteMany({});
  for (const number of roomNumbers) {
    await prismaClient.room.create({
      data: {
        id: number,
        hasDesk: Math.random() > 0.5,
        hasProjector: Math.random() > 0.7,
        hasWifi: Math.random() > 0.1,
        seets: getRandomInt(10, 50),
      },
    });
  }

  await prismaClient.user.deleteMany({});
  let i = 0;
  while (i <= 15) {
    i++;
    await prismaClient.user.create({
      data: {
        email: faker.internet.email(),
        name: faker.name.findName(),
        password: faker.internet.password(),
      },
    });
  }
  console.log("done");

  process.exit();
}
