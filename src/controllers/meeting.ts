import { Request, Response } from "express";

import { meetingService } from "../services/meetingService";
import { roomService } from "../services/roomService";
import { userService } from "../services/userService";

const meetingController = {
  async create(req: Request, res: Response) {
    const time = Number(req.body.time);
    const roomId = Number(req.body.roomId);
    const room = await roomService.getRoom(roomId);
    if (!room) {
      res.sendStatus(404);
    } else {
      const isFree = await roomService.isFree(time, room);
      if (!isFree) {
        res.sendStatus(400);
      } else {
        console.log({ room, time }, req.user);
        const newMeeting = await meetingService.create(room, time, req.user);
        res.json(newMeeting);
      }
    }
  },

  async getAll(req: Request, res: Response) {
    const meetings = await meetingService.getInvites(req.user);
    res.json(meetings);
  },

  async getOne(req: Request, res: Response) {
    const id = Number(req.params.id);
    const meeting = await meetingService.getMeeting(id);
    res.json(meeting);
  },

  async invite(req: Request, res: Response) {
    const id = Number(req.params.id);
    const meeting = await meetingService.getMeeting(id);

    const inviteeId = Number(req.query.inviteeId);
    const invitee = await userService.getById(inviteeId);

    if (!meeting || !invitee) {
      res.sendStatus(404);
    } else {
      const invite = await meetingService.invite(meeting, req.user, invitee);
      if (invite) {
        res.json(invite);
      } else {
        res.sendStatus(400);
      }
    }
  },

  async changeInviteStatus(req: Request, res: Response) {
    const id = Number(req.query.id);
    const inviteeId = Number(req.query.inviteeId);
    const status = String(req.query.status);
    console.log({ id, inviteeId, status });
    // @ts-ignore
    const invite = await meetingService.changeStatus(id, inviteeId, status);
    res.json(invite);
  },
};

export { meetingController };
