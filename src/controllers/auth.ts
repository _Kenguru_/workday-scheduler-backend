import { Request, Response } from "express";

import { UserDto } from "../dto/user";
import { userService } from "../services/userService";

const authController = {
  async signup(req: Request, res: Response) {
    const { email, password, name } = <UserDto>req.body;
    console.log({ email, password, name });
    try {
      const user = await userService.create(email, password, name);
      console.log(user);
      delete user.password;
      const token = userService.createToken(user);
      res.json({ ...user, token });
    } catch (err) {
      res.statusCode = 500;
      res.json({ errors: [{ msg: err.toString() }] });
    }
  },

  async signin(req: Request, res: Response) {
    const { email, password } = <Omit<UserDto, "name">>req.body;
    try {
      const user = await userService.getByEmail(email);
      const isPasswordCorrect = await userService.checkPassword(user, password);
      if (!isPasswordCorrect) {
        res.statusCode = 401;
        res.json({ errors: [{ msg: "Неверный пароль" }] });
      } else {
        delete user.password;
        const token = userService.createToken(user);
        res.json({ ...user, token });
      }
    } catch (err) {
      res.statusCode = 500;
      res.json({ errors: [{ msg: err.toString() }] });
    }
  },
};

export { authController };
