import { Request, Response } from "express";

import { roomService } from "../services/roomService";
import { meetingService } from "../services/meetingService";

const roomController = {
  async getAll(req: Request, res: Response) {
    const time = Number(req.query.time);
    const roomsPromise = roomService.getRooms();
    const meetingsPromise = meetingService.getAllByTime(time);
    const [rooms, meetings] = await Promise.all([
      roomsPromise,
      meetingsPromise,
    ]);
    const result = rooms.map((room) => ({
      ...room,
      isFree: !meetings.some((m) => m.roomId === room.id),
    }));
    res.json(result);
  },
};

export { roomController };
