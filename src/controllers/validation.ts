import { Request, Response, NextFunction } from "express";
import { validationResult } from "express-validator";

export const validationController = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.statusCode = 400;
    res.json({ errors: errors.array() });
  } else {
    next();
  }
};
