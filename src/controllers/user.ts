import { Request, Response } from "express";

import { UserDto } from "../dto/user";
import { userService } from "../services/userService";

const userController = {
  async getAll(req: Request, res: Response) {
    const users = await userService.getAll();
    res.json(users);
  },
};

export { userController };
