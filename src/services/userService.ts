import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import { User } from "@prisma/client";
import { prismaClient as prisma } from "./prismaClient";

class UserService {
  async create(email: string, password: string, name: string) {
    const hashedPassword = await bcrypt.hash(password, 6);
    return prisma.user.create({
      data: { email, name, password: hashedPassword },
    });
  }

  async getById(id: number) {
    const user = await prisma.user.findOne({ where: { id } });
    if (!user) throw Error("User not found");
    return user;
  }

  async getByEmail(email: string) {
    const user = await prisma.user.findOne({ where: { email } });
    if (!user) throw Error("User not found");
    return user;
  }

  async getAll() {
    const users = await prisma.user.findMany({
      select: {
        id: true,
        name: true,
        email: true,
        invitations: { include: { meeting: true } },
      },
    });
    return users.map((user) => {
      // @ts-ignore
      user.busyAt = user.invitations.map((inv) => inv.meeting.time);
      delete user.invitations;
      return user;
    });
  }

  checkPassword(user: User, password: string) {
    return bcrypt.compare(password, user.password);
  }

  createToken(user: User) {
    delete user.password;
    return jwt.sign(user, "secret", { expiresIn: "100d" });
  }

  verifyToken(user: User, token: string) {
    try {
      const decoded = <User>jwt.verify(token, "secret");
      return decoded.id === user.id;
    } catch {
      return false;
    }
  }

  async checkIfEmailFree(email: string) {
    const user = await prisma.user.findOne({ where: { email } });
    return !user;
  }
}

export const userService = new UserService();
