import { Room } from "@prisma/client";

import { prismaClient as prisma } from "./prismaClient";
import { meetingService } from "./meetingService";

let rooms: Room[];

class RoomService {
  async getRooms() {
    if (!rooms) {
      rooms = await prisma.room.findMany();
    }
    return rooms;
  }

  async getRoom(id: number) {
    const rooms = await this.getRooms();
    return rooms.find((room) => room.id === id);
  }

  async isFree(time: number, room: Room) {
    const meetings = await meetingService.getAllByTime(time);
    return !meetings.some((meeting) => meeting.roomId === room.id);
  }
}

export const roomService = new RoomService();
