import { Room, Meeting, User } from "@prisma/client";

import { prismaClient as prisma } from "./prismaClient";

class MeetingService {
  create(room: Room, time: number, creator: User) {
    return prisma.meeting.create({
      data: {
        time,
        creator: { connect: { id: creator.id } },
        room: { connect: { id: room.id } },
        invitations: {
          create: {
            invitee: { connect: { id: creator.id } },
            inviter: { connect: { id: creator.id } },
            status: "accepted",
          },
        },
      },
    });
  }

  async changeStatus(
    meetingId: number,
    userId: number,
    status: "accepted" | "pending" | "declined"
  ) {
    const meeting = await this.getMeeting(meetingId);
    console.log("got meeting", meeting, { meetingId });
    if (meeting) {
      const invite = meeting.invitations.find((i) => i.inviteeId === userId);
      if (invite) {
        return prisma.invitation.update({
          where: { id: invite.id },
          data: { status },
        });
      }
    }
    return false;
  }

  async isUserBusy(user: User, time: number) {
    const userMeetings = await this.getInvites(user, true);
    return userMeetings.some((m) => m.meeting.time === time);
  }

  async invite(meeting: Meeting, inviter: User, invitee: User) {
    const userBusy = await this.isUserBusy(invitee, meeting.time);
    return userBusy
      ? false
      : prisma.invitation.create({
          data: {
            meeting: { connect: { id: meeting.id } },
            inviter: { connect: { id: inviter.id } },
            invitee: { connect: { id: invitee.id } },
          },
        });
  }

  async getInvites(user: User, onlyAccepted = false) {
    const invites = await prisma.invitation.findMany({
      where: { inviteeId: user.id },
      include: {
        meeting: {
          include: {
            creator: {
              select: {
                id: true,
                name: true,
                password: false,
              },
            },
          },
        },
      },
    });
    return onlyAccepted
      ? invites.filter((i) => i.status === "accepted")
      : invites;
  }

  getMeeting(id: number) {
    return prisma.meeting.findOne({
      where: { id },
      include: {
        invitations: {
          include: {
            inviter: { select: { id: true, name: true } },
            invitee: { select: { id: true, name: true } },
          },
        },
        room: true,
      },
    });
  }

  getAllByTime(time: number) {
    return prisma.meeting.findMany({ where: { time } });
  }

  async getStats() {
    // return prisma.queryRaw`select "roomId", count("roomId") from "Meeting" GROUP BY "roomId" order by "count" DESC;`;
    const meetings = await prisma.meeting.findMany();
    // @ts-ignore
    return meetings.reduce((acc, cur) => {
      // @ts-ignore
      if (acc[cur.id]) {
        // @ts-ignore
        acc[cur.id] += 1;
      } else {
        // @ts-ignore
        acc[cur.id] = 1;
      }
    }, {});
  }
}

export const meetingService = new MeetingService();
