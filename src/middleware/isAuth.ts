import jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from "express";
import { Room, Meeting, User } from "@prisma/client";

import {} from "../services/userService";

class AuthError extends Error {
  message = "Not authorized";
  statusCode = 401;
}

export async function isAuth(req: Request, res: Response, next: NextFunction) {
  try {
    const [, token] = req.get("Authorization")!.split(" ");
    const decodedUser = <User>jwt.verify(token, "secret");
    if (!decodedUser) throw new AuthError();
    req.user = decodedUser;
    next();
  } catch (err) {
    next(err);
  }
}
