import express from "express";
import bodyParser from "body-parser";
import cors from "cors";

import { authRoutes } from "./routes/auth";
import { meetingsRoutes } from "./routes/meetings";
import { roomsRoutes } from "./routes/rooms";
import { userRoutes } from "./routes/users";

const app = express();
app.use(cors());

app.use(bodyParser.json());

app.use("/auth", authRoutes);
app.use("/meetings", meetingsRoutes);
app.use("/rooms", roomsRoutes);
app.use("/users", userRoutes);

app.listen(process.env.PORT || 3000, () => console.log("App started"));
