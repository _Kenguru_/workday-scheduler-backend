import express from "express";
import { body } from "express-validator";

import { userService } from "../services/userService";
import { authController } from "../controllers/auth";
import { validationController } from "../controllers/validation";

const authRoutes = express.Router();

authRoutes.post("/signup", [
  body("email")
    .isEmail()
    .withMessage("Некорректный электронный адрес!")
    .custom(async (value, { req }) => {
      if (value) {
        const isEmailFree = await userService.checkIfEmailFree(value);
        if (!isEmailFree) throw Error("Эта электронная почта уже занята!");
      }
    }),
  body("password").trim().isLength({ min: 5 }),
  body("name").trim().notEmpty(),
  validationController,
  authController.signup,
]);

authRoutes.post("/signin", [
  body("email").trim(),
  body("password").trim(),
  validationController,
  authController.signin,
]);

export { authRoutes };
