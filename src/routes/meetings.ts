import express from "express";
import { body, param, query } from "express-validator";

import { isAuth } from "../middleware/isAuth";
import { meetingController } from "../controllers/meeting";
import { validationController } from "../controllers/validation";

const meetingsRoutes = express.Router();

meetingsRoutes.post("/", [
  isAuth,
  body("time").isIn([9, 10, 11, 12, 14, 15, 16]),
  body("roomId").isInt(),
  validationController,
  meetingController.create,
]);

meetingsRoutes.get("/", [isAuth, meetingController.getAll]);

meetingsRoutes.get("/:id", [
  isAuth,
  param("id").isInt(),
  validationController,
  meetingController.getOne,
]);

meetingsRoutes.post("/change_invite_status", [
  isAuth,
  query("id").isInt(),
  query("inviteeId").isInt(),
  query("status").isIn(["accepted", "declined"]),
  validationController,
  meetingController.changeInviteStatus,
]);

meetingsRoutes.post("/:id([0-9]+)", [
  isAuth,
  param("id").isInt(),
  query("inviteeId").isInt(),
  validationController,
  meetingController.invite,
]);

export { meetingsRoutes };
