import express from "express";

import { isAuth } from "../middleware/isAuth";
import { roomController } from "../controllers/room";

const roomsRoutes = express.Router();

roomsRoutes.get("/", isAuth, roomController.getAll);

export { roomsRoutes };
